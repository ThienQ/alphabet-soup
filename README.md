# Specification

I wrote this program in Python, it should work directly out of the box as long as you have 'input.txt' in the same folder as this python script. The program is documented pretty well but feel free to contact me if you have any further questions.

- Email: <nguyenth496@gmail.com>
- Phone: [443 857 8447](tel:+4438578447)

Thanks,

Thien Nguyen