# -*- coding: utf-8 -*-
"""
Created on Mon May  3 23:55:10 2021

@author: Thien Nguyen

This is my implementation for the alphabet soup found at: https://gitlab.com/enlighten-challenge/alphabet-soup
"""

def parse_data(data_path):
    """
    This function parse data from a .txt file into a 2d List. Each element
    is a line in the .txt, and each line is a List of characters. 
    
    This function will return two variables. The parsed data, and a List that 
    contains all the keyword to look for. 

    Parameters
    ----------
    data_path : String
        Path to where the .txt file is to parse

    Returns
    -------
    data : List
        The 2D list of all the parsed data
    key_words: List
        The 1D list of all keywords to look for
    """
    data = []
    key_words = []
    ignored_char = ['\n', ' ']
    file = open(data_path, "r")
    num_row = None
    for row, line in enumerate(file):
        raw_row_data = [d[1] for d in enumerate(line)]  # the raw format with \n and space included
        final_row_data = [d for d in raw_row_data if d not in ignored_char]  # modified to remove \n and spaces
        if row == 0:  # If first line, grab first char since that tell us how many rows there are
            num_row = int(final_row_data[0])
        elif row <= num_row:  # loop until we hit max number of rows
            data.append(final_row_data)
        else:  # anything afterward is just keywords
            key_words.append(line[:-1])
        
    return data, key_words

def is_word_valid(data, word, position):
    """
    This function will return True or False if the word and position inputted is a valid word. 
    This function works by looking at the position of the first character in the word. Look around
    to try and find the second character. If the second character is found successfully, record
    the direction, and loop through the rest of the word with the direction and make sure 
    the rest of the words matched. This function will print out the word, starting state, 
    and ending state if it successfully found the word

    Parameters
    ----------
    data : List
        The 2D list of all the parsed data
    word : String
        The word to check if the position is valid
    position : Tuple
        The position of the first char, format is (row, column)

    Returns
    -------
    bool
        If the position of the first character will flow to a successful word. 

    """
    row_len = len(data)
    col_len = len(data[0])
    original_position = str(position[0]) + ":" + str(position[1])
    
    if len(word) == 1:  # if the word is length of 1
        print(word, original_position, original_position)
        return True
    
    for row_direction in [-1, 0, 1]:
        final_row = position[0] + row_direction
        if final_row < 0 or final_row >= row_len:  # ignore out of index list range
            continue
        for col_direction in [-1, 0, 1]:
            final_col = position[1] + col_direction
            if final_col < 0 or final_col >= col_len:  # ignore out of index list range
                continue
           
            if data[final_row][final_col] == word[1]:  # if the second char is within range, record the direction
                direction = (final_row - position[0], final_col - position[1])
                current_position = (position[0] + direction[0], position[1] + direction[1])
                for i in range(2, len(word)):  # loop through the rest of char and make sure the rest match according to the direction
                    new_position = (current_position[0] + direction[0], current_position[1] + direction[1])
                    if word[i] != data[new_position[0]][new_position[1]]:
                        return False
                    current_position = new_position
                
                # If the code reaches here, that mean all the word match and we found out starting|ending position!
                new_position = str(current_position[0]) + ":" + str(current_position[1])
                print(word, original_position, new_position)
                return True

    return False

def find_word(data, word):
    """
    This function will loop through the data and try to find the position of the character
    that matched the first letter of the word. If it does, it pass the position, the word, and 
    the data into is_word_valid. if is_word_valid return True, we know we successfully found
    the correct path and no need to search the word anymore. If False, continue searching

    Parameters
    ----------
    data : List
        The 2D list of all the parsed data
    word : String
        The word to check if the position is valid

    Returns
    -------
    None.

    """
    for row_idx in range(len(data)):
        for col_idx in range(len(data[row_idx])):
            if data[row_idx][col_idx] == word[0]:  # Looking for the first matched char
                position = (row_idx, col_idx)
                if is_word_valid(data, word, position):  # If the position of the first matched char is valid, exit the function
                    return
        
        
if __name__ == '__main__':
    data, key_words = parse_data('input.txt')
    for word in key_words:
        find_word(data, word)
